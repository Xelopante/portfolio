let info = {
  name: "Lucas Chevalot",
  logo_name: "Lucas",
  flat_picture: require("./src/assets/me.png"),
  config: {
    use_cookies: true,
    navbar: {
      blur: false
    }
  },
  description:
    "I am currently working at Cirkwi, a company which proposes itineraries on a map for web or mobile services, in the context of my block release web development degree. I like to try new technologies/ programming languages (if time permits) and troubleshoot peoples' IT problems (I learn a lot from that). I am working on a pair project with a classmate for the moment and used to run a personnal micro business as a web developper, which gave me a good opportunity to improve my self teaching skills. My past professional experiences have led me to be more into the backend part, but I also do some frontend development.",
  links: {
    linkedin: "https://fr.linkedin.com/in/lucas-chevalot-791567206",
    gitlab: "https://gitlab.com/Xelopante",
    resume: "https://drive.google.com/file/d/1ha7Z0t0w2Sc10ZtYDAlvf0uNN_7Y2re6/view?usp=sharing",
  },
  education: [
    {
      name: "Charlemagne IUT",
      place: "Nancy, France",
      date: "Sept, 2021 - present",
      degree: "bachelor in web development",
      description:
        "",
      skills: [
        "Web programming",
        "Front-end development",
        "Back-end development",
      ]
    },
    {
      name: "Raymond-Poincaré high school",
      place: "Bar le Duc, France",
      date: "Sept, 2019 - June, 2021",
      degree: "BTEC Higher National Diploma",
      description:
        "",
      skills: [
        "Web programming",
        "Back-end development",
        "Database administration",
      ]
    }
  ],
  experience: [
    {
      name: "Cirkwi",
      place: "Nancy, France",
      date: "Sept, 2021 - present",
      position: "web developer",
      description:
        "",
      skills: ["HTML5", "CSS3", "PHP", "JavaScript", "Symphony", "MySQL", "Scss"]
    },
    {
      name: "LC Info (self micro-business)",
      place: "France",
      date: "May 2021 - Aug 2021",
      position: "web developper",
      description:
        "",
      skills: ["HTML5", "CSS3", "PHP", "Symphony", "MySQL", "Prestashop", "Python"]
    },
    {
      name: "Fetch World",
      place: "France",
      date: "Jan, 2021 - Febr, 2021",
      position: "web developper, web designer",
      description:
        "",
      skills: ["HTML5", "CSS3", "PHP", "Symphony", "MySQL", "Prestashop", "Photoshop"]
    }
  ],
  skills: [
    {
      title: "Programming languages",
      info: [
        "Python","PHP","C#","MySQL"],
      icon: "fas fa-file-code"
    },
    {
      title: "Front-end",
      info: [
        "HTML5","CSS3", "SCSS","Javascript Es6"],
      icon: "fa fa-code"
    },
    {
      title: "Web technologies",
      info: ["Vue", "Slim", "Eloquent ORM"],
      icon: "fas fa-laptop-code"
    },
    {
      title: "Operating Systems",
      info: [
        "Windows", "GNU/Linux"],
      icon: "fas fa-server"
    },
    {
      title: "Conception",
      info: [
        "Merise Modelisation", "UML"],
      icon: "fa fa-cubes"
    },
    {
      title: "Languages",
      info: [
        "English (Fluent)", "French (Native)"],
      icon: "fas fa-language"
    },
  ],
  hobbies: [
    {
      name: "Cooking",
      icon: "fas fa-cookie"
    },
    {
      name: "Reading books",
      icon: "fa fa-book"
    }
  ],
  portfolio: [
    {
      name: "Docto map’",
      pictures: [
      ],
      technologies: ["JavaScript", "Ajax", "REST API", "PHP", "MySQL"],
      category: "Web App",
      date: "March, 2021 - 30 days",
      github:
        "",
      visit: "",
      description:
        "A responsive Doctolib’-like website, where you can find doctors in Paris on a map. You can book appointments, list them and edit their upcoming date. This project is based on my first homemade REST API !"
    },
    {
      name: "Co’op",
      pictures: [
      ],
      technologies: ["JavaScript", "VueJS", "REST API"],
      category: "Web App",
      date: "November, 2021 - 30 days",
      github:
        "",
      visit: "",
      description:
        "A forum application inspired by Slack. It’s a Vue.js-based web app, on which you can create conversations allowing users to post messages about it. It is also possible to delete, edit your messages/conversations, and even list all users and their 10 last messages"
    },
    {
      name: "Le bon sandwich",
      pictures: [
      ],
      technologies: ["PHP", "Slim", "REST API", "MySQL", "Docker"],
      category: "Web App",
      date: "November, 2022 - 60 days",
      github:
        "",
      visit: "",
      description:
        "Another web app based on a homemade REST API, allowing its users to list a company’s sandwiches and order them. Another interface is made for the administrator to edit/delete or post sandwiches on websites."
    },
    {
      name: "J’achète en local",
      pictures: [
      ],
      technologies: ["JavaScript", "Prestashop", "Python", "Symphony", "PHP", "MySQL"],
      category: "Web App",
      date: "May, 2021 - 90 days",
      github:
        "",
      visit: "",
      description:
        "In the context of my micro business, I had the opportunity to create an e-commerce website for each French department, allowing local shopkeepers from their departments to show and sell their products online. These products would eventually be displayed on a global website that i developed to agregate every local website and allow a global search throught them"
    },
    {
      name: "Show Time",
      pictures: [
      ],
      technologies: ["C#", "ASP.NET", "REST API", "MySQL"],
      category: "Web App",
      date: "November, 2021 - 40 days",
      github:
        "",
      visit: "",
      description:
        "A Xamarin Android application which lists the watched movies/tv shows, their seasons and episodes for each users."
    },
    {
      name: "Let's Cook",
      pictures: [
      ],
      technologies: ["C#", "MySQL"],
      category: "Web App",
      date: "October, 2021 - 20 days",
      github:
        "",
      visit: "",
      description:
        "A C# application that allow users to add ingredients and creates recipes with them, the recipes ingredients quantity is automaticaly calculated depending on the amount of people eating."
    },
  ],
};

export default info;
